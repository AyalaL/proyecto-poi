﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NuntiusBLL;
using NuntiusDAL; 

namespace NuntiusApplication
{
    public partial class Chat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["loggedUser"] != null)
            {
                UserProfile loggedUserProfile = new UserProfile();

                loggedUserProfile = (UserProfile)Session["loggedUser"];

                

                //string name = dtUsuario.Rows[0]["username"].ToString();

                lbl_Name.Text = "Bienvenido devuelta, " + loggedUserProfile.name + ".";
                lbl_Username.Text = "O debería decir, " + loggedUserProfile.username + ".";
                lbl_Email.Text = "¿Es tu correo: " + loggedUserProfile.email + "? ¿Sí? Genial.";
                lbl_Rank.Text = "Bueno, tu rango es " + loggedUserProfile.rank.ToString() + ".";
            }
            else
            {
                lbl_Username.Text = "Nada que ver aquí, trata de registrarte y luego iniciar sesión.";
            }
        }
    }
}