﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NuntiusDAL;
using NuntiusBLL;

namespace NuntiusApplication
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {



        }

        protected void btnRegister_ServerClick(object sender, EventArgs e)
        {
            RegisterUserProfile();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Alta", "alert('Cuenta registrada exitosamente.')", true);
            Response.Redirect("~/Login.aspx");
        }


        public void RegisterUserProfile()
        {
            UserProfileBLL userProfile_BLL = new UserProfileBLL();
            UserProfile userProfileToRegister = new UserProfile();

            userProfileToRegister.name = txt_Name.Text;
            userProfileToRegister.username = txt_Username.Text;
            userProfileToRegister.email = txt_Email.Text;
            userProfileToRegister.password = txt_Password.Text;
            userProfileToRegister.profilePic = txt_ProfilePic.Text;
            userProfileToRegister.rank = 1;


            try
            {

                if(!string.Equals(txt_Password.Text, txt_PasswordR.Text))
                {
                    throw new Exception("Las contraseñas señaladas no coinciden");
                }
                else
                {
                    userProfile_BLL.RegisterUserProfile(userProfileToRegister);
                }

            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Alta", "alert('" + ex.Message + "')", true);
            }


        }


        //public void AgregarFacultad()
        //{

        //    Facultad_BLL facultadBLL = new Facultad_BLL();
        //    Facultad facultad = new Facultad();

        //    facultad.codigo = txtCodigo.Text;
        //    facultad.nombre = txtNombre.Text;
        //    facultad.fechaCreacion = Convert.ToDateTime(txtFechaCreacion.Text);
        //    facultad.universidad = int.Parse(ddlUniversidad.SelectedValue);
        //    facultad.ciudad = int.Parse(ddlCiudad.SelectedValue);


        //    MateriaFacultad materiaFacultad;
        //    List<MateriaFacultad> listMaterias = new List<MateriaFacultad>();

        //    try
        //    {
        //        foreach (ListItem item in lbxMaterias.Items)
        //        {
        //            if (item.Selected)
        //            {
        //                materiaFacultad = new MateriaFacultad();
        //                materiaFacultad.materia = int.Parse(item.Value);
        //                materiaFacultad.facultad = facultad.ID_Facultad;
        //                listMaterias.Add(materiaFacultad);
        //            }
        //        }

        //        facultadBLL.AgregarFacultad(facultad, listMaterias);
        //        limpiarCampos();

        //        DataTable dtFacultades = new DataTable();
        //        dtFacultades = (DataTable)ViewState["tablaFacultades"];
        //        dtFacultades.Rows.Add(facultad.codigo, facultad.nombre);

        //        grd_facultad.DataSource = dtFacultades;
        //        grd_facultad.DataBind();

        //    }
        //    catch (Exception ex)
        //    {
        //        Page.ClientScript.RegisterStartupScript(this.GetType(), "Alta", "alert('" + ex.Message + "')", true);
        //    }
        //}
    }
}