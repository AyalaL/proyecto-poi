﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin;
using Owin;

//Clase con dependencia necesaria para inicializar SignalR.
[assembly: OwinStartup(typeof(NuntiusApplication.Startup))]
namespace NuntiusApplication
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}