﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="NuntiusApplication.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SignalR Chat : Register</title>
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="Content/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="Content/NuntiusStyle.css" rel="stylesheet" />
    <%--    <link href="Content/icheck-bootstrap.css" rel="stylesheet" />--%>
</head>
<body class="hold-transition register-page">
    <div class="bg">
        <div class="scroll center">
            <form class="center" id="form1" runat="server">

                <div class="register-box">
                    <div class="register-logo">
                        <a href="Login.aspx"><b>Nuntius </b>Applicación de Mensajería medieval </a>
                    </div>

                    <div class="register-box-body">
                        <p class="login-box-msg">Register a new membership</p>

                        <div class="form-group has-feedback">
                            <asp:TextBox ID="txt_Name" MaxLength="50" runat="server" type="text" class="form-control" required="required" placeholder="Nombre completo"></asp:TextBox>
                            <span class="fas fa-user-circle form-control-feedback"></span>
                            <asp:RequiredFieldValidator ID="rfv_Name" runat="server" ControlToValidate="txt_Name"
                                ValidationGroup="vlg1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group has-feedback">
                            <asp:TextBox ID="txt_Username" MaxLength="50" runat="server" type="text" class="form-control" required="required" placeholder="Mote"></asp:TextBox>
                            <span class="fas fa-crown form-control-feedback"></span>
                            <asp:RequiredFieldValidator ID="rfv_Username" runat="server" ControlToValidate="txt_Username"
                                ValidationGroup="vlg1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group has-feedback">
                            <asp:TextBox ID="txt_ProfilePic" MaxLength="255" runat="server" type="text" class="form-control" required="required" placeholder="Foto de perfil"></asp:TextBox>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            <asp:RequiredFieldValidator ID="rfv_ProfilePic" runat="server" ControlToValidate="txt_ProfilePic"
                                ValidationGroup="vlg1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group has-feedback">
                            <asp:TextBox ID="txt_Email" MaxLength="255" runat="server" type="email" class="form-control" required="required" placeholder="E-mail"></asp:TextBox>
                            <span class="fas fa-envelope-open form-control-feedback"></span>
                            <asp:RequiredFieldValidator ID="rfv_Email" runat="server" ControlToValidate="txt_Email"
                                ValidationGroup="vlg1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group has-feedback">
                            <asp:TextBox ID="txt_Password" MaxLength="255" runat="server" type="password" class="form-control white-Color" required="required" placeholder="Contraseña"></asp:TextBox>
                            <span class="fas fa-eye form-control-feedback"></span>
                            <asp:RequiredFieldValidator ID="rfv_Password" runat="server" ControlToValidate="txt_Password"
                                ValidationGroup="vlg1"></asp:RequiredFieldValidator>
                        </div>

                        <div class="form-group has-feedback">
                            <asp:TextBox ID="txt_PasswordR" MaxLength="255" runat="server" type="password"  class="form-control" required="required" placeholder="Confirme contraseña"></asp:TextBox>
                            <span class="fas fa-hands-helping form-control-feedback"></span>
                            <asp:RequiredFieldValidator ID="rfv_PasswordR" runat="server" ControlToValidate="txt_PasswordR"
                                ValidationGroup="vlg1"></asp:RequiredFieldValidator>
                        </div>

                        <div class="row">

                            <!-- /.col -->
                            <div class="col-xs-4">
                                <asp:Button ID="Button1" type="submit" class="btn btn-primary btn-block btn-flat" runat="server" Text="Registrar" OnClick="btnRegister_ServerClick" ValidationGroup="vlg1" />
                                <%--                                <button type="submit" class="btn btn-primary btn-block btn-flat" id="btnRegister" runat="server" onserverclick="btnRegister_ServerClick">Register</button>--%>
                            </div>
                            <!-- /.col -->
                        </div>

                        <a href="Login.aspx" class="text-center">I already have an account</a>
                    </div>
                    <!-- /.form-box -->
                </div>

            </form>
        </div>
    </div>
    <%--    <script src="Scripts/jquery-3.3.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>--%>
</body>
</html>
