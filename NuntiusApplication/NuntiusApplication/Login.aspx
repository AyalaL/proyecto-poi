﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="NuntiusApplication.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SignalR Chat : Login</title>
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="Content/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <link href="Content/icheck-bootstrap.css" rel="stylesheet" />
    <link href="Content/NuntiusStyle.css" rel="stylesheet" />
</head>
<body class="hold-transition login-page">
    <div class="bg">
        <div class="scroll center">
            <form id="form1" class="center" runat="server">
                <div class="login-box">
                    <div class="login-logo">
                        <a href="Login.aspx"><b>Nuntius </b>Applicación de Mensajería medieval</a>
                    </div>
                    <!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg">Please Login to Proceed</p>


                        <div class="form-group has-feedback">
                            <asp:TextBox ID="txt_Email" MaxLength="255" runat="server" type="text" class="form-control" required="required" placeholder="E-mail"></asp:TextBox>
                            <span class="fas fa-envelope-open form-control-feedback"></span>
                            <asp:RequiredFieldValidator ID="rfv_Email" runat="server" ControlToValidate="txt_Email"
                                ValidationGroup="vlg1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group has-feedback">
                            <asp:TextBox ID="txt_Password" MaxLength="255" runat="server" type="text" TextMode="Password" class="form-control" required="required" placeholder="Contraseña"></asp:TextBox>
                            <span class="fas fa-eye form-control-feedback"></span>
                            <asp:RequiredFieldValidator ID="rfv_Password" runat="server" ControlToValidate="txt_Password"
                                ValidationGroup="vlg1"></asp:RequiredFieldValidator>
                        </div>

                        <div class="row">

                            <!-- /.col -->
                            <div class="col-xs-4">
                                <asp:Button ID="btnSignIn" runat="server" Text="Login"  OnClick="btnSignIn_Click" CssClass="btn btn-success btn-block btn-flat" /><br />

                            </div>

                            <div class="col-xs-8">
                                <a href="Register.aspx" class="btn btn-primary btn-block btn-flat">Nuevo usuario? Regístrate</a>

                            </div>
                            <!-- /.col -->
                        </div>


                    </div>
                    <!-- /.login-box-body -->
                </div>
            </form>
        </div>
    </div>
    <script src="plugins/jquery-1.9.1.min"></script>
    <script src="plugins/bootstrap.min.js"></script>

</body>
</html>
