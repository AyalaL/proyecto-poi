﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NuntiusDAL;
using NuntiusBLL;

namespace NuntiusApplication
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }


        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            if (IsValidUser())
            {
                Response.Redirect("~/Chat.aspx");
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Sesión", "alert('El usuario o la contraseña es inválida.')", true);
            }

        }

        public bool IsValidUser()
        {
            bool access = false;

            UserProfileBLL userProfile_BLL = new UserProfileBLL();
            UserProfile loggedUserProfile = new UserProfile();

            loggedUserProfile = userProfile_BLL.SelectUserProfileLogin(txt_Email.Text, txt_Password.Text);

            if (loggedUserProfile != null)
            {
                Session["loggedUser"] = loggedUserProfile;
                access = true;

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Alta", "alert('"+ Session["loggedUser"].ToString() +"')", true);
            }

            return access;

        }


    }
}