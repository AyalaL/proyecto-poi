﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuntiusDAL
{
    public class UserProfileDAL
    {
        //Creo una instancia de el modelo entidead de la base de datos LINQ.
        NuntiusDBEntities NuntiusModel;

        //La inicializo en el contructor.
        public UserProfileDAL()
        {
            NuntiusModel = new NuntiusDBEntities();
        }

        //Usando LINQ, se agrega un usuario que se mande y luego guarda. Mucho más simple que un select desde la base de datos.
        public void RegisterUserProfile(UserProfile user)
        {
            NuntiusModel.UserProfile.Add(user);
            NuntiusModel.SaveChanges();
        }

        //Pido un perfil de usuario si uno en la base de datos conincide con el id enviado.
        public UserProfile SelectUserProfileByID(int ID_User)
        {
            //Se almacena en user el objeto que cumpla con las condiciones.
            var userIDMatch = (from mUserProfile in NuntiusModel.UserProfile //De todos los perfiles de usuario en el modelo, toma uno por uno,
                        where mUserProfile.ID_User == -ID_User //checa si coincide id's
                        select mUserProfile).FirstOrDefault(); //y selecciona el primero de los resultados o si no hay, regrésame nulo.
            return userIDMatch;
        }

        //Pido un perfil de usuario si uno en la base de datos coninciden nombre y contraseña-
        public UserProfile SelectUserProfileByEmail(string email)
        {
            //Se almacena en user el objeto que cumpla con las condiciones.
            var userEmailMatch = (from mUserProfile in NuntiusModel.UserProfile //De todos los perfiles de usuario en el modelo, toma uno por uno,
                                  where mUserProfile.email == email //checa si coincide los emails
                                  select mUserProfile).FirstOrDefault(); //y selecciona el primero de los resultados o si no hay, regrésame nulo.
            return userEmailMatch;
        }

        //Pido un perfil de usuario si uno en la base de datos coninciden nombre y contraseña-
        public UserProfile SelectUserProfileLogin(string email, string password)
        {
            //Se almacena en user el objeto que cumpla con las condiciones.
            var userLoginMatch = (from mUserProfile in NuntiusModel.UserProfile //De todos los perfiles de usuario en el modelo, toma uno por uno,
                               where mUserProfile.email == email && mUserProfile.password == password //checa si coincide id's y nombres
                               select mUserProfile).FirstOrDefault(); //y selecciona el primero de los resultados o si no hay, regrésame nulo.
            return userLoginMatch;
        }

        //Pido un perfil de usuario si uno en la base de datos coninciden nombre y contraseña-
        public UserProfile SelectUserProfileByUsername(string username)
        {
            //Se almacena en user el objeto que cumpla con las condiciones.
            var userLoginMatch = (from mUserProfile in NuntiusModel.UserProfile //De todos los perfiles de usuario en el modelo, toma uno por uno,
                                  where mUserProfile.username == username //checa si coincide id's y nombres
                                  select mUserProfile).FirstOrDefault(); //y selecciona el primero de los resultados o si no hay, regrésame nulo.
            return userLoginMatch;
        }



        public void UpdateUserProfile(UserProfile user)
        {
            UserProfile userToModify = SelectUserProfileByID(user.ID_User);

            userToModify.name = user.name;
            userToModify.username = user.name;
            userToModify.email = user.name;
            userToModify.password = user.name;
            userToModify.profilePic = user.name;
            userToModify.rank = user.rank;

            NuntiusModel.SaveChanges();
        }

        public void DeleteUserProfile(int ID_User)
        {
            UserProfile userToDelete = SelectUserProfileByID(ID_User);

            NuntiusModel.UserProfile.Remove(userToDelete);
            NuntiusModel.SaveChanges();
        }

    }
}
