﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuntiusDAL;

namespace NuntiusBLL
{
    //Clase exclusivamente para implementación de validaciones o reglas de lado del servicdor. BLL: Business Logic Layer.
    public class UserProfileBLL
    {

        public void RegisterUserProfile(UserProfile user)
        {
            UserProfileDAL userProfile_DAL = new UserProfileDAL();
            UserProfile existingUser = new UserProfile();

            existingUser = SelectUserProfileByEmail(user.email);

            if(existingUser != null)
            {
                throw new Exception("El correo ya se encuentra en la base de datos");
            }
            else
            {
                existingUser = SelectUserProfileByUsername(user.username);

                if (existingUser != null)
                {
                    throw new Exception("El mote ya se encuentra en la base de datos");
                }
                else
                {
                    userProfile_DAL.RegisterUserProfile(user);
                }

            }

        }

        public UserProfile SelectUserProfileByUsername(string username)
        {
            UserProfileDAL userProfile_DAL = new UserProfileDAL();
            return userProfile_DAL.SelectUserProfileByUsername(username);
        }

        public UserProfile SelectUserProfileByID(int ID_User)
        {
            UserProfileDAL userProfile_DAL = new UserProfileDAL();
            return userProfile_DAL.SelectUserProfileByID(ID_User);
        }

        public UserProfile SelectUserProfileByEmail(string email)
        {
            UserProfileDAL userProfile_DAL = new UserProfileDAL();
            return userProfile_DAL.SelectUserProfileByEmail(email);
        }

        public UserProfile SelectUserProfileLogin(string email, string password)
        {
            UserProfileDAL userProfile_DAL = new UserProfileDAL();
            return userProfile_DAL.SelectUserProfileLogin(email, password);
        }

    }
}
